module gitlab.com/c0va23/redirector

go 1.13

require (
	github.com/BurntSushi/toml v0.3.0
	github.com/PuerkitoBio/purell v1.1.0 // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/asaskevich/govalidator v0.0.0-20180315120708-ccb8e960c48f // indirect
	github.com/blang/vfs v0.0.0-20170207135758-c14afcac1725 // indirect
	github.com/bluele/factory-go v0.0.0-20180415032208-14d56da9cb7c
	github.com/corpix/uarand v0.0.0-20170903190822-2b8494104d86 // indirect
	github.com/daaku/go.zipexe v0.0.0-20150329023125-a5fe2436ffcb // indirect
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/docker/go-units v0.3.3 // indirect
	github.com/go-openapi/analysis v0.0.0-20180710011727-3c8fe72ed5d3 // indirect
	github.com/go-openapi/errors v0.0.0-20180515155515-b2b2befaf267
	github.com/go-openapi/jsonpointer v0.0.0-20180322222829-3a0015ad55fa // indirect
	github.com/go-openapi/jsonreference v0.0.0-20180322222742-3fb327e6747d // indirect
	github.com/go-openapi/loads v0.0.0-20171207192234-2a2b323bab96
	github.com/go-openapi/runtime v0.0.0-20180628220156-9a3091f566c0
	github.com/go-openapi/spec v0.0.0-20180710175419-bce47c9386f9
	github.com/go-openapi/strfmt v0.0.0-20180703152050-913ee058e387
	github.com/go-openapi/swag v0.0.0-20180703152219-2b0bd4f193d0
	github.com/go-openapi/validate v0.0.0-20180703152151-9a6e517cddf1
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/jessevdk/go-flags v1.4.0
	github.com/kardianos/osext v0.0.0-20170510131534-ae77be60afb1 // indirect
	github.com/levenlabs/golib v0.0.0-20180911183212-0f8974794783 // indirect
	github.com/mailru/easyjson v0.0.0-20180606163543-3fdea8d05856 // indirect
	github.com/mediocregopher/radix.v2 v0.0.0-20180603022615-94360be26253
	github.com/mitchellh/mapstructure v0.0.0-20180511142126-bb74f1db0675 // indirect
	github.com/onsi/ginkgo v1.10.2 // indirect
	github.com/onsi/gomega v1.7.0 // indirect
	github.com/oschwald/geoip2-golang v1.3.0 // indirect
	github.com/oschwald/maxminddb-golang v1.5.0 // indirect
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/phogolabs/parcello v0.0.0-20180518134247-bae01a3ceb41
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rs/cors v1.4.0
	github.com/sirupsen/logrus v1.0.5
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.2.2
	golang.org/x/crypto v0.0.0-20180621125126-a49355c7e3f8 // indirect
	golang.org/x/net v0.0.0-20180906233101-161cd47e91fd
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce // indirect
)

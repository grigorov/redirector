package resolvers

import (
	"time"

	"gitlab.com/c0va23/redirector/models"
)

// RuleResolver used for resolve host and path to HTTP code and new URL
type RuleResolver func(models.Rule, string) *models.Target

// HostRulesResolver is interface host HostRules resolver
type HostRulesResolver interface {
	Resolve(models.HostRules, string) models.Target
}

// DefaultResolvers is default resolvers for MultiHostRulesResolver
var DefaultResolvers = map[string]RuleResolver{
	"simple":  SimpleResolver,
	"pattern": PatternResolver,
}

// MultiHostRulesResolver is wrapper for multple resolver
type MultiHostRulesResolver map[string]RuleResolver

// Resolve match each rule by resolver and return if it return target.
// Otherwise return default target.
func (r MultiHostRulesResolver) Resolve(
	hostRules models.HostRules,
	sourcePath string,
) models.Target {
	currentTime := time.Now()

	for _, rule := range hostRules.Rules {
		if rule.ActiveFrom != nil && (time.Time)(*rule.ActiveFrom).After(currentTime) {
			continue
		}

		if rule.ActiveTo != nil && (time.Time)(*rule.ActiveTo).Before(currentTime) {
			continue
		}

		for resolverType, resolver := range r {
			if resolverType != rule.Resolver {
				continue
			}

			if target := resolver(rule, sourcePath); nil != target {
				return *target
			}
		}
	}

	return hostRules.DefaultTarget
}

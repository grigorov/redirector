package resolvers_test

import (
	"net/http"
	"testing"
	"time"

	"gitlab.com/c0va23/redirector/models"
	"gitlab.com/c0va23/redirector/resolvers"
	"gitlab.com/c0va23/redirector/testutils/factories"

	"github.com/go-openapi/strfmt"
	"github.com/stretchr/testify/assert"
)

func TestResolve_WithoutResolvers(t *testing.T) {
	a := assert.New(t)

	hostRules := factories.HostRulesFactory.MustCreate().(models.HostRules)
	emptyResolvers := resolvers.MultiHostRulesResolver{}

	a.Equal(
		hostRules.DefaultTarget,
		emptyResolvers.Resolve(hostRules, "/"),
	)
}

func TestResolve_ResolveNotMatch(t *testing.T) {
	a := assert.New(t)

	rule := factories.RuleFactory.MustCreate().(models.Rule)
	hostRules := factories.HostRulesFactory.
		MustCreateWithOption(map[string]interface{}{
			"Rules": []models.Rule{rule},
		}).(models.HostRules)

	ruleResolver := func(rule models.Rule, sourcePath string) *models.Target {
		return nil
	}

	hostRulesResolver := resolvers.MultiHostRulesResolver{
		"fakeResolver": ruleResolver,
	}

	a.Equal(
		hostRules.DefaultTarget,
		hostRulesResolver.Resolve(hostRules, "/"),
	)
}

func TestResolve_ResolveMatch(t *testing.T) {
	a := assert.New(t)

	rule := factories.RuleFactory.MustCreate().(models.Rule)
	hostRules := factories.HostRulesFactory.
		MustCreateWithOption(map[string]interface{}{
			"Rules": []models.Rule{rule},
		}).(models.HostRules)

	ruleResolver := func(rule models.Rule, sourcePath string) *models.Target {
		return &rule.Target
	}

	hostRulesResolver := resolvers.MultiHostRulesResolver{
		rule.Resolver: ruleResolver,
	}

	a.Equal(
		rule.Target,
		hostRulesResolver.Resolve(hostRules, "/"),
	)
}

func TestResolve_ActiveResolveMatch(t *testing.T) {
	a := assert.New(t)

	prevTime := strfmt.DateTime(time.Now().Add(time.Hour * -24))
	futureTime := strfmt.DateTime(time.Now().Add(time.Hour * 24))

	resolverType := "test_resolver"

	ruleActiveFrom := models.Rule{
		ActiveFrom: &futureTime,
		SourcePath: "/test",
		Resolver:   resolverType,
		Target: models.Target{
			HTTPCode: http.StatusMovedPermanently,
			Path:     "/active_from",
		},
	}

	ruleActiveTo := models.Rule{
		ActiveTo:   &prevTime,
		SourcePath: "/test",
		Resolver:   resolverType,
		Target: models.Target{
			HTTPCode: http.StatusMovedPermanently,
			Path:     "/active_to",
		},
	}
	expectedRule := models.Rule{
		SourcePath: "/test",
		ActiveFrom: &prevTime,
		ActiveTo:   &futureTime,
		Resolver:   resolverType,
		Target: models.Target{
			HTTPCode: http.StatusMovedPermanently,
			Path:     "/target",
		},
	}

	hostRules := factories.HostRulesFactory.
		MustCreateWithOption(map[string]interface{}{
			"Rules": []models.Rule{
				ruleActiveFrom,
				ruleActiveTo,
				expectedRule,
			},
		}).(models.HostRules)

	ruleResolver := func(rule models.Rule, sourcePath string) *models.Target {
		return &rule.Target
	}

	hostRulesResolver := resolvers.MultiHostRulesResolver{
		resolverType: ruleResolver,
	}

	target := hostRulesResolver.Resolve(hostRules, "/test")

	a.Equal(expectedRule.Target, target)
}

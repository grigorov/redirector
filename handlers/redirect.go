package handlers

import (
	"io"
	"net/http"
	"strings"

	"golang.org/x/net/idna"

	"gitlab.com/c0va23/redirector/log"
	"gitlab.com/c0va23/redirector/models"
	"gitlab.com/c0va23/redirector/resolvers"
	"gitlab.com/c0va23/redirector/store"
)

const locationHeader = "Location"

const (
	notFoundMessage            = "Not found"
	internalServerErrorMessage = "Internal server error"
)

var redirectLogger = log.NewLeveledLogger("RedirectHandler")

// RedirectHandler is handler for redirect requests
type RedirectHandler struct {
	store.Store
	resolvers.HostRulesResolver
}

// NewRedirectHandler build new RedirectHandler
func NewRedirectHandler(
	store store.Store,
	resolver resolvers.HostRulesResolver,
) *RedirectHandler {
	return &RedirectHandler{
		Store:             store,
		HostRulesResolver: resolver,
	}
}

// ServeHTTP handle all requests for redirect.
// If target not found, then respond with 404 code.
// If store return error, then respond with 500 code.
func (rh *RedirectHandler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	hostParts := strings.Split(req.Host, ":")
	host := hostParts[0]

	var hostRules *models.HostRules

	encodedHost, err := idna.ToUnicode(host)
	if err == nil {
		hostRules, err = rh.GetHostRules(encodedHost)
	}

	switch err {
	case nil:
		target := rh.Resolve(*hostRules, req.RequestURI)
		rw.Header().Add(locationHeader, target.Path)
		rw.WriteHeader(int(target.HTTPCode))
	case store.ErrNotFound:
		rw.WriteHeader(http.StatusNotFound)
		writeBody(rw, notFoundMessage)
	default:
		redirectLogger.WithError(err).Errorf("Redirect error: %s", err)
		rw.WriteHeader(http.StatusInternalServerError)
		writeBody(rw, internalServerErrorMessage)
	}
}

func writeBody(rw io.Writer, message string) {
	_, err := rw.Write(([]byte)(message))
	if err != nil {
		redirectLogger.
			WithError(err).
			Errorf("Error on write not found body: %s", err)
	}
}

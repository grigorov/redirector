# Build binary stage

FROM golang:1.13-alpine as builder

ENV SRC_PATH=/app

WORKDIR ${SRC_PATH}

RUN apk add --update \
    make \
    git \
    curl \
    || true

ADD Makefile ${SRC_PATH}
ADD VERSION ${SRC_PATH}

RUN make install-codegen-tools

ADD . ${SRC_PATH}

ARG GOOS=linux
ARG GOARCH=amd64

RUN make bin/redirector-server-${GOOS}-${GOARCH}

# Build image stage

FROM alpine:3.9

ARG GOOS=linux
ARG GOARCH=amd64
ARG version=latest
ARG git_commit

COPY --from=builder /app/bin/redirector-server-${GOOS}-${GOARCH} /usr/local/bin/redirector-server

LABEL maintainer="Dmitrij Fedorenko <c0va23@gmail.com>"
LABEL version="${version}"
LABEL git.commit="${git_commit}"

ENV PORT=8080

EXPOSE ${PORT}

CMD redirector-server \
    --port ${PORT} \
    --basic-username ${USERNAME} \
    --basic-password ${PASSWORD} \
    --host 0.0.0.0

package validators

import (
	"fmt"
	"regexp"

	"golang.org/x/net/idna"

	"gitlab.com/c0va23/redirector/locales"
	"gitlab.com/c0va23/redirector/models"
)

var hostRegex = regexp.MustCompile(`^(?i:[a-z0-9]+(-+[a-z0-9]+)*($|\.))+`)

const maxHostLength = 255

// HostRules field names
const (
	FieldHostRulesHost          = "host"
	FieldHostRulesDefaultTarget = "defaultTarget"
	FieldHostRulesRules         = "rules"
)

// ValidateHostRules with all embedded structs
func ValidateHostRules(hostRules models.HostRules) (
	modelError models.ModelValidationError,
) {
	modelError = validateHost(modelError, hostRules.Host)

	if defaultTargetError := ValidateTarget(hostRules.DefaultTarget); defaultTargetError != nil {
		modelError = addEmbedError(modelError, FieldHostRulesDefaultTarget, defaultTargetError)
	}

	for index, rule := range hostRules.Rules {
		if ruleError := ValidateRule(rule, hostRules.Rules[:index]); ruleError != nil {
			modelError = addEmbedError(
				modelError,
				fmt.Sprintf("%s.%d", FieldHostRulesRules, index),
				ruleError,
			)
		}
	}

	return
}

func validateHost(modelError models.ModelValidationError, host string) models.ModelValidationError {
	if host == "" {
		modelError = addFieldError(modelError, FieldHostRulesHost, locales.ErrorsRequired)
	}

	host, _ = idna.ToASCII(host)

	if len(host) > maxHostLength {
		modelError = addFieldError(modelError, FieldHostRulesHost, locales.ErrorsHostRulesHostTooLong)
	}

	if !hostRegex.MatchString(host) {
		modelError = addFieldError(modelError, FieldHostRulesHost, locales.ErrorsHostRulesHostInvalidPattern)
	}

	return modelError
}

PACKAGE := gitlab.com/c0va23/redirector
ERRCHECK_EXCLUDE_PATTERN := '(cmd/redirector-server|restapi/operations)'

GOSWAGGER_VERSION := v0.20.1
GOSWAGGER_VERSION_PATH := bin/swagger_${GOSWAGGER_VERSION}

BUILD_ARCH := $(shell go env GOARCH)
BUILD_OS := $(shell go env GOOS)

GOARCH := $(shell go env GOARCH)
GOOS := $(shell go env GOOS)

GOLANGCI_LINT_VERSION := 1.20.0
GOLANGCI_LINT_ARCHIVE_NAME := golangci-lint-${GOLANGCI_LINT_VERSION}-${GOOS}-${GOARCH}
GOLANGCI_LINT_URL := https://github.com/golangci/golangci-lint/releases/download/v${GOLANGCI_LINT_VERSION}/${GOLANGCI_LINT_ARCHIVE_NAME}.tar.gz

FULL_VERSION := $(shell cat VERSION)

SWAGGER_TARGETS := \
	restapi/server.go \
	restapi/doc.go \
	restapi/operations/ \
	cmd/redirector-server/ \
	models/locales.go \
	models/locale_translations.go \
	models/model_validation_error.go \
	models/server_error.go \
	models/translation.go \
	models/validation_error.go

GENERATE_TARGES := \
	vendor/ \
	locales/resource.go \
	locales/constants.go \
	$(SWAGGER_TARGETS)

SOURCE_FILES := \
	$(shell git ls-files *.toml *.go) \
	$(GENERATE_TARGES)

export PATH := $(PWD)/bin:$(PATH)

bin/${GOLANGCI_LINT_ARCHIVE_NAME}/:
	mkdir -p bin
	curl -L ${GOLANGCI_LINT_URL} | tar --directory bin/ --gzip --extract --verbose

bin/golangci-lint: bin/${GOLANGCI_LINT_ARCHIVE_NAME}/
	ln -f -s $(PWD)/bin/${GOLANGCI_LINT_ARCHIVE_NAME}/golangci-lint $@
	touch $@

install-codegen-tools:
	go get -u github.com/phogolabs/parcello/cmd/parcello

vendor/: go.mod go.sum $(SWAGGER_TARGETS)
	go mod vendor

$(SWAGGER_TARGETS): bin/swagger api.yml
	bin/swagger generate server \
		--model FieldValidationError \
		--model Locales \
		--model LocaleTranslations \
		--model ModelValidationError \
		--model ServerError \
		--model Translation \
		--model ValidationError \
		-f api.yml

locales/resource.go: locales/*.toml ./locales/build.go
	go generate ./locales/build.go

locales/constants.go: locales/*.toml locales/locales.go scripts/locales_constants/
	go generate ./locales/locales.go

lint: $(GENERATE_TARGES) bin/golangci-lint
	golangci-lint run ./...

test: $(GENERATE_TARGES)
	go test -cover ./...

bin/:
	mkdir bin/ || true

${GOSWAGGER_VERSION_PATH}: | bin/
	curl -o $@ -L https://github.com/go-swagger/go-swagger/releases/download/$(GOSWAGGER_VERSION)/swagger_${BUILD_OS}_$(BUILD_ARCH)
	chmod +x $@

bin/swagger: ${GOSWAGGER_VERSION_PATH} | bin/
	ln -sf $(PWD)/${GOSWAGGER_VERSION_PATH} $@

bin/redirector-server-${GOOS}-$(GOARCH): $(SOURCE_FILES) | bin/
	go build -o $@ $(PACKAGE)/cmd/redirector-server

# Utils

clean:
	git clean -f -d -X  -- **
	git clean -f -d -X  -- cmd/**

bump-minor-version:
	./scripts/bump_version/main.go VERSION minor

bump-patch-version:
	./scripts/bump_version/main.go VERSION patch

commit-version:
	git commit VERSION -m "Bump version $(FULL_VERSION)"

tag-version:
	git tag $(FULL_VERSION)

tag-push:
	git push origin $(FULL_VERSION)
